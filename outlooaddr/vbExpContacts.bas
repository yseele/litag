Attribute VB_Name = "vbOutlookExtractContact"
Option Explicit
' Thanks for your contibution mind

'---------------------------------------------------------------------------------------
' About based source code is below
'
' Author    : Daniel Pineault, CARDA Consultants Inc.
' Website   : http://www.cardaconsultants.com
' Purpose   : Extract contact information from Outlook
' Copyright : The following is release as Attribution-ShareAlike 4.0 International
'             (CC BY-SA 4.0) - https://creativecommons.org/licenses/by-sa/4.0/
' Req'd Refs: Uses Late Binding, so none required
'
' Usage:
' ~~~~~~
' Call Outlook_ExtractContacts
'
' Revision History:
' Rev       Date(yyyy/mm/dd)        Description
' **************************************************************************************
' 1         2019-07-15              Initial Release - Forum Help
'---------------------------------------------------------------------------------------

Sub ExpContacts()

    Dim oOut             As Object
    Dim oNmSp            As Object
    Dim oFld             As Object
    Dim oItm             As Object
    Dim oPrp             As Object
    
    Const olFolderContacts = 10
    Const olContact = 40
 
    On Error Resume Next
    Set oOut = GetObject(, "Outlook.Application")
    If Err.Number <> 0 Then
        Err.Clear
        Set oOut = CreateObject("Outlook.Application")
    End If
    On Error GoTo Error_Handler
 
    Set oNmSp = oOut.GetNamespace("MAPI")
    Set oFld = oNmSp.GetDefaultFolder(olFolderContacts)
 
    Open Environ("HOMEPATH") & "\dirtyfile.csv" For Output As #1
    Write #1, "名", "姓", "会社名", "部署", "役職", "電子メール アドレス", "電子メール表示名"
 
    On Error Resume Next
    For Each oItm In oFld.Items
        
        With oItm
            If .Class = olContact Then
                Debug.Print TypeName(oItm)
                
                'Debug.Print .FirstName, .LastName, .Email1Address, .Email1DisplayName, .CompanyName, .Department, .JobTitle
                Write #1, .FirstName, .LastName,
                Write #1, .CompanyName, .Department, .JobTitle,
                Write #1, .Email1Address, .Email1DisplayName
                '.FullName, .FirstName, .LastName, .CompanyName
'                For Each oPrp In .ItemProperties
'                    Debug.Print , oPrp.Name, oPrp.Value
'                Next oPrp
            End If
        End With
    Next oItm
    
    Close #1
 
Error_Handler_Exit:

    On Error Resume Next
    If Not oPrp Is Nothing Then Set oPrp = Nothing
    If Not oItm Is Nothing Then Set oItm = Nothing
    If Not oFld Is Nothing Then Set oFld = Nothing
    If Not oNmSp Is Nothing Then Set oNmSp = Nothing
    If Not oOut Is Nothing Then Set oOut = Nothing
    Close #1
    Exit Sub
 
Error_Handler:
    MsgBox Err.Description
    Resume Error_Handler_Exit
End Sub

